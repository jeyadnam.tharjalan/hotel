<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Menu;
use Faker\Generator as Faker;

$factory->define(App\Models\Menu::class, function (Faker $faker) {
    return [
        'name' =>$faker->word, //faker is the library wich will help us to push random data to database.
        'description'=>$faker->paragraphs(2,true),
        'price' =>$faker->numberBetween(99,999), 
    ];
});
